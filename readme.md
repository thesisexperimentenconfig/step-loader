# Configuration

The root directory of your project should contain a file named `config.js`. In this file 
there should be

	module.exports = {
    	'updateServiceURL': "http://urlforyourupdateservice/steps"
	}

# Usage

the method `requestModelsFromServer()` is used to retrieve models from the server.
A promise is returned. For more information see [q, a library for promises](https://www.npmjs.com/package/q).

#Tests
After Downloading
    npm install
    npm install -g istanbul

run the command
    npm test
	
in terminal.