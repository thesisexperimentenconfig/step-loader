var stepLoader = require('../index');

var should = require('chai').should();
var expect = require('chai').expect;
var nock = require('nock');

var steps = require('./steps');
var updateService = nock('http://devices.com')
						.get('/steps')
						.reply(200, steps);
						


describe('Test retrieval from server with mock', function(){
	this.timeout(15000);
	
	it('#getModels can access server', function(done){
		stepLoader
			.getModels()
			.then(function(models){
				JSON.stringify(models).should.equal(JSON.stringify(steps));
				done();
			});
	});
	
	it('#getModels server invalid statusCode', function(done){
		nock.cleanAll();
		
		updateService = nock('http://devices.com')
						.get('/steps')
						.reply(400);
						
		stepLoader
			.getModels()
			.then(function(models){
				JSON.stringify(models).should.equal(JSON.stringify(steps));
				done();
			});
	});
});