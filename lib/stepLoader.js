// file system access
var fs = require('fs');
var path = require('path');
var request = require('request');
// promises to force synchronicity.
var Q = require('q');

var dns = require('dns');

module.exports = {



    getModels: function () {
        var d = Q.defer();
        this.requestModelsFromServer().then(function (models) {
            d.resolve(models);
        });

        return d.promise;
    },

    /*
     * Requests the models from the server.
     */
    requestModelsFromServer: function () {
        var self = this;
        var d = Q.defer();
        var fileLoc = path.normalize(__dirname + "/steps.json");
        // load the configuration file
        var configLocation = path.normalize(process.cwd() + '/config.js');
        var config = require(configLocation);
        this.checkInternetConnectivity().then(function (isConnected) {
            if (isConnected) {

                request.get(config.updateServiceURL, function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        var json = JSON.parse(body);
                        self.writeFile(json);
                        d.resolve(json);

                    } else {
                        var models = fs.readFileSync(fileLoc);
                        var models = JSON.parse(models);
                        
                        
                        d.resolve(models);
                    }
                })
            } else {
                var models = fs.readFileSync(fileLoc);
                var models = JSON.parse(models);
                d.resolve(models);
            }
        });

        return d.promise;
    },


    writeFile: function (body) {
        var fileLoc = path.normalize(__dirname + "/steps.json");
        fs.writeFileSync(fileLoc, JSON.stringify(body));
    },

    /*
     * Checks whether the computer is connected to the internet.
     * uses promises. Resolves to true if computer is connected to the
     * internet, to false otherwise.
     */
    checkInternetConnectivity: function () {
        var d = Q.defer();
        dns.lookup('google.com', function (err) {
            if (err && err.code == "ENOTFOUND") {
                d.resolve(false);
            } else {
                d.resolve(true);
            }
        })

        return d.promise;
    }
}